#!/bin/bash

# ==============================================
#
# Copyright (C) 2018 Funtap System
#
# ==============================================

# SET VARIABLES
echo -n " username = "
read username

echo -n " password = "
read password

echo -n " domain = "
read domain

echo -n " username upload apk = "
read username_apk

echo -n " password upload apk = "
read password_apk

NGINX_RELEASE_VERSION="1.16.0"
USER="$username"
PASSWORD="$password"
DOMAIN="$domain"
USER_APK=$username_apk
PASSWORD_APK=$password_apk

useradd $USER
echo "$PASSWORD" | passwd --stdin $USER

useradd $USER_APK
echo "$PASSWORD_APK" | passwd --stdin $USER_APK

if [ ! -d /home/$USER ]; then
mkdir /home/$USER
fi

# Change timezone
mv /etc/localtime /etc/localtime.bak
ln -s /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime

# Disable selinux
sudo sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
setenforce 0

# Change port SSH to 8622 and turn on password login
sed -i --follow-symlinks 's/#Port 22/Port 8622/g' /etc/ssh/sshd_config
sed -i --follow-symlinks 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config

service sshd restart

##########################################
##########Compile Nginx with options######
##########################################

# Install require packages
yum -y  install  yum-utils pcre pcre-devel  rsyslog zip unzip gcc gcc-c++ openssl openssl-devel zlib gd freetype freetype-devel autoconf openldap openldap-devel bzip2-devel libpng libpng-devel libjpeg libjpeg-devel perl-ExtUtils-Embed GeoIP GeoIP-devel

cd SRC/
tar -zxvf nginx-${NGINX_RELEASE_VERSION}.tar.gz
cd nginx-${NGINX_RELEASE_VERSION}

./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/conf/nginx.conf --http-log-path=/var/log/nginx/access.log --error-log-path=/var/log/nginx/error.log --without-mail_pop3_module --without-mail_imap_module --without-mail_smtp_module --without-http_split_clients_module --without-http_uwsgi_module --without-http_scgi_module --with-http_realip_module --with-http_secure_link_module --with-http_stub_status_module --with-http_geoip_module --with-http_gzip_static_module --with-debug --with-file-aio --with-threads --with-http_addition_module --with-http_perl_module --with-ld-opt=-Wl,-E --with-http_ssl_module --with-http_stub_status_module --with-http_v2_module

make -j4
make install

rm -rfv nginx-${NGINX_RELEASE_VERSION}

cd ..

cp nginx.init /etc/init.d/nginx

chmod +x /etc/init.d/nginx

cp -f nginx.conf /etc/nginx/conf/nginx.conf
sed -i --follow-symlinks "s/user nginx/user $USER/g" /etc/nginx/conf/nginx.conf

mkdir -p /etc/nginx/conf.d/ /etc/nginx/sites-available/ /data/cdn/public /data/cdn/apk
chown -R $USER:$USER /data/cdn/public
chown -R $USER_APK:$USER_APK /data/cdn/apk
cp -f rotate_nginx /etc/logrotate.d/nginx
cp -f cdn.conf /etc/nginx/sites-available/
sed -i --follow-symlinks "s/root_dir/$USER/g" /etc/nginx/sites-available/cdn.conf
sed -i --follow-symlinks "s/domain.com/$DOMAIN/g" /etc/nginx/sites-available/cdn.conf 

echo INDEX WORKED!!! >> /data/cdn/public/index.html

chown -R $USER:$USER /home/$USER
 

##########################################
##########Install FTP with vsftpd#########
##########################################

yum install vsftpd ftp -y

# Create config file

mv /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.bak

cat <<EOT >> /etc/vsftpd/vsftpd.conf
anonymous_enable=NO
local_enable=YES
write_enable=YES
local_umask=022
dirmessage_enable=YES
xferlog_enable=YES
connect_from_port_20=YES
xferlog_std_format=YES
ascii_upload_enable=YES
ascii_download_enable=YES
ftpd_banner=Welcome to blah FTP service.
listen=NO
listen_ipv6=YES
pam_service_name=vsftpd
userlist_enable=YES
tcp_wrappers=YES
use_localtime=YES
pasv_min_port=12000
pasv_max_port=12500
chroot_local_user=YES
chroot_list_enable=YES
chroot_list_file=/etc/vsftpd.chroot_list
allow_writeable_chroot=YES
EOT

touch /etc/vsftpd.chroot_list
usermod -d /data/cdn/public/ $USER
usermod -d /data/cdn/apk/ $USER_APK

# Startup service

chkconfig vsftpd on
chkconfig nginx on

# Start service
useradd nginx
service nginx start
service vsftpd start

